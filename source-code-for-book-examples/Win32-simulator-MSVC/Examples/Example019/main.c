/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示你演示了在中断服务函数中使用
		xQueueReceiveFromISR 从队列中接收消息，以及用
		xQueueSendToBackFromISR向队列发送消息。
		这里要注意的是各个任务的运行态阻塞态的切换.
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 本示例模拟中断号的使用。中断号0到2,用于FreeRTOS windows移植版系统使用。
所以第一个可以用的应用中断号是3 */
#define mainINTERRUPT_NUMBER	3

/* 要创建的任务 */
static void vIntegerGenerator( void *pvParameters );
static void vStringPrinter( void *pvParameters );

/* 这个函数模拟中断服务函数。这就是那个任务要同步的中断。 */
static uint32_t ulExampleInterruptHandler( void );

/*-----------------------------------------------------------*/

/* 声明两个QueueHandle_t类型变量，
一个队列用于从中断中读取值，另一个向中断中写入值 */
QueueHandle_t xIntegerQueue, xStringQueue;

int main( void )
{
    /* 队列在使用之前必须先创建，本例创建两个队列。
	一个队列持有uint32_t类型，另一个持有char*类型
	两个队列都可以最大持有10个数据项。在真实应用中
	应检测队列创建的返回值，以确保队列创建成功。 */
    xIntegerQueue = xQueueCreate( 10, sizeof( uint32_t ) );
	xStringQueue = xQueueCreate( 10, sizeof( char * ) );

	/* 创建一个任务，向中断服务程序中传递整数，该任务的优先级是1 */
	xTaskCreate( vIntegerGenerator, "IntGen", 1000, NULL, 1, NULL );

	/* 创建一个任务,用于输出来自中断服务程序的字符串，该任务的优先级是2*/
	xTaskCreate( vStringPrinter, "String", 1000, NULL, 2, NULL );

	/* 安装软中断处理函数句柄，这里的使用方式是基于FreeRTOS的移植实现。
	这种使用方式只是在FreeRTOS Windows上使用的，这里只是模拟中断  */
	vPortSetInterruptHandler( mainINTERRUPT_NUMBER, ulExampleInterruptHandler );

	/* 开启调度，使得任务被执行. */
	vTaskStartScheduler();

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void vIntegerGenerator( void *pvParameters )
{
TickType_t xLastExecutionTime;
const TickType_t xDelay200ms = pdMS_TO_TICKS( 200UL ), xDontBlock = 0;
uint32_t ulValueToSend = 0;
BaseType_t i;

	/* 调用 vTaskDelayUntil() 该函数初始化变量. */
	xLastExecutionTime = xTaskGetTickCount();

	for( ;; )
	{
		/* 这是一个周期性任务。阻塞它直到再次运行，该任务每200ms执行一次. */
		vTaskDelayUntil( &xLastExecutionTime, xDelay200ms );

		/* 发送5个数字给队列，每个值都比前一个值大1.
		这些数值将被中断服务函数读取。由于中断服务程序总是把队列清空，
		所以这个任务不需要指定阻塞时间，就能保证可以把5个值都写入到队列中 */
		for( i = 0; i < 5; i++ )
		{
			xQueueSendToBack( xIntegerQueue, &ulValueToSend, xDontBlock );
			ulValueToSend++;
		}

		/* 产生中断，从而使得中断服务程序能从队列中读取数据。
		这里产生软中断的方式与FreeRTOS的移植实现相关。
		这里的使用方式只适用于FreeRTOS的windows移植版本。
		这里只能是模拟产生中断.*/
		vPrintString( "Generator task - About to generate an interrupt.\r\n" );
		vPortGenerateSimulatedInterrupt( mainINTERRUPT_NUMBER );
		vPrintString( "Generator task - Interrupt generated.\r\n\r\n\r\n" );
	}
}
/*-----------------------------------------------------------*/

static void vStringPrinter( void *pvParameters )
{
char *pcString;

	for( ;; )
	{
		/* 阻塞队列直到数据到达. */
		xQueueReceive( xStringQueue, &pcString, portMAX_DELAY );

		/* 输出接收到的字符串 */
		vPrintString( pcString );
	}
}
/*-----------------------------------------------------------*/

static uint32_t ulExampleInterruptHandler( void )
{
BaseType_t xHigherPriorityTaskWoken;
uint32_t ulReceivedNumber;

/* 这里的字符串声明为static const(静态常量值)，以确保它们不会在中断服务程序
栈空间分配的，并且即使中断服务程序不再执行了，它们的埴仍然存在。 */
static const char *pcStrings[] =
{
	"String 0\r\n",
	"String 1\r\n",
	"String 2\r\n",
	"String 3\r\n"
};

	/* 与通常一样，xHigherPriorityTaskWoken要被初始化为空。以
	判定它是否被中断API安全函数置为pdTRUE */
	xHigherPriorityTaskWoken = pdFALSE;

	/* 从队列读取数据，直到队列为空. */
	while( xQueueReceiveFromISR( xIntegerQueue, &ulReceivedNumber, &xHigherPriorityTaskWoken ) != errQUEUE_EMPTY )
	{
		/* 把接收数据转换成其最后两位的值。然后用转换后的值做为the pcStrings[]数据的下标，
		来选择一个将要发送的字符串指针(char *) */
		ulReceivedNumber &= 0x03;
		xQueueSendToBackFromISR( xStringQueue, &pcStrings[ ulReceivedNumber ], &xHigherPriorityTaskWoken );
	}

    /* 如果从xIntegerQueue 队列中接收一个值，将导致一个任务退出阻塞状态，
	并且如果离开阻塞状态的任务的优先级高于运行状态的优先级,那么
	xHigherPriorityTaskWoken变量的值将会在xQueueReceiveFromISR()中被置为pdTRUE.

	如果发送到xStringQueue队列，将导致一个任务离开阻塞态，如果离开阻塞态的任务
	的优先级高于运行状态的优先级，那么xHigherPriorityTaskWoken就已经在
	xQueueSendFromISR()函数中置为pdTRUE了.

	xHigherPriorityTaskWoken这个值做为portYIELD_FROM_ISR()函数的参数。
	如果xHigherPriorityTaskWoken的值是pdTRUE,那么调用portYIELD_FROM_ISR()
	将会请求上下文切换。
	如果xHigherPriorityTaskWoken的值仍是pdFALSE,那么调用portYIELD_FROM_ISR()
	将不会产生任务影响

	在windows中，该函数的portYIELD_FROM_ISR()实现，包含返回语句。
	所以这里就不显式返回一个值了。 */
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}









