/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示例主要是要演示
		在不同任务的优先级任务中，通过消息队列来进行消息传递的功能。
		即高优先级的任务，通过队列等待时间，进入阻塞状态，使得低优先级的
		任务可以获取执行时间，将将消息发送到队列中。从而实现任务间消息的
		传递。并且我们要注意到两个发送任务的优先级是相同的，因此它们的消息
		将轮流发送到队列，这是因为当优先级相同时，使用的是时间片轮流调度算法。
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/* 示例包含头文件 */
#include "supporting_functions.h"


/* 将要创建的任务，两个发送任务实例，一个接收任务实例 */
static void vSenderTask( void *pvParameters );
static void vReceiverTask( void *pvParameters );

/*-----------------------------------------------------------*/

/* 声明一个QueueHandle_t 类型的变量，
用于存储三个任务使用的队列信息 */
QueueHandle_t xQueue;


int main( void )
{
    /* 创建一个队列，最多可以保存5个长整型变量. */
    xQueue = xQueueCreate( 5, sizeof( int32_t ) );

	if( xQueue != NULL )
	{
		/* 创建两个任务，用于往任务写入消息.  传递给它们的参数将会被写入到队列,
		所以任务将不断的将100写入队列，同时另一个任务不断将200写入队列。两个任
		务的优先级都是1 */
		xTaskCreate( vSenderTask, "Sender1", 1000, ( void * ) 100, 1, NULL );
		xTaskCreate( vSenderTask, "Sender2", 1000, ( void * ) 200, 1, NULL );

		/* 创建一个从队列读取消息的任务.  
		本任务的优先级是2，所以高于前两个发送的任务的优先级. */
		xTaskCreate( vReceiverTask, "Receiver", 1000, NULL, 2, NULL );

		/* 开启调度，使得任务被执行. */
		vTaskStartScheduler();
	}
	else
	{
		/* 队列没有被创建成功. */
	}

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void vSenderTask( void *pvParameters )
{
int32_t lValueToSend;
BaseType_t xStatus;

	/* 创建了该任务的两个实例，所以发送给队列的值是通过参数传递过来的，
	而不是写死的硬编码。这样每个实例使用不同的值。转换参数类型。*/
	lValueToSend = ( int32_t ) pvParameters;

	/* 跟大多数任务一样，这个任务的实现是一个无限循环 */
	for( ;; )
	{
		/* 第一个参数是接收数据的队列(queue)，
		该队列是在任务调度之前创建的，所以在本任务开始之前就可以执行
		第二个参数是要发送的数据的地址.
		第三个参数是阻塞时间，即当队列为满的时候，阻塞当前任务一段时间，
		直到队列有空间可以写入消息.
		本例中我们使用了一个特殊的阻塞时间，因为队列总是可用的
		（因为写入队列的任务优先级低，读队列的任务优先级高，
		所以读取消息快，发送消息慢，因此队列总是可用的）。*/
		xStatus = xQueueSendToBack( xQueue, &lValueToSend, 0 );

		if( xStatus != pdPASS )
		{
			/* 当队列满时，我们不能往里写入消息。这里肯定是一个错误 ，
			因为队列不可能持有多于1个消息项! */
			vPrintString( "Could not send to the queue.\r\n" );
		}
	}
}
/*-----------------------------------------------------------*/

static void vReceiverTask( void *pvParameters )
{
/* 声明一个变量接收队列中的值 */
int32_t lReceivedValue;
BaseType_t xStatus;
const TickType_t xTicksToWait = pdMS_TO_TICKS( 500UL );

	/* 该任务仍然是一个无限循环. */
	for( ;; )
	{
		/* 由于当数据写入队列时，本任务立即解除阻塞，所以这里的调用
		返回值应该永远是0,即队列为空。(因为消息被本任务取走,看下面的代码)
		uxQueueMessagesWaiting 该函数是查询队列中有几个消息项 */
		
		if( uxQueueMessagesWaiting( xQueue ) != 0 )
		{
			vPrintString( "Queue should have been empty!\r\n" );
		}

		/* 第一个参数是队列，将从该参数中获取数据.该队列先于任务调度之前创建
		因此在该任务运行之前，是其第一次运行。

		第二个参数是接收数据的存放地址,本例中该参数是一个变量的地址，该变量的址
		址空间满足接收数据所要求的空间大小。
		第三个参数是阻塞时间;即如果队列为空，保持该任务为阻塞状态，直到队列可用的最长等待时间 
		（如果队列中有消息，那么就不会等待，如果队列为空，则最大等待时间为xTicksToWait,
		等待超时，若队列仍然为空，也将退出等待）
		*/
		xStatus = xQueueReceive( xQueue, &lReceivedValue, xTicksToWait );

		if( xStatus == pdPASS )
		{
			/* 从队列成功接收数据，并将打印输出它的值 */
			vPrintStringAndNumber( "Received = ", lReceivedValue );
		}
		else
		{
			/* 我们在等待100ms后，仍然没有收到任何消息。这必然是一个错误，因为发送任务
			是自由运行的，并且持续写入消息到队列。*/
			vPrintString( "Could not receive from the queue.\r\n" );
		}
	}
}






