/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示例仍然演示定时器的使用。不过本示例演示了多个定时器
		同时使用同一个回调函数时，如何区分是那个定时器调用了该回
		调函数，以及如何停止自动重新加载软定时器，
		以及注意事项：即不要在回调中调用引起RTOS守护任务阻塞的函数
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 单次触发和自动重入定时器的定时周期 */
#define mainONE_SHOT_TIMER_PERIOD		( pdMS_TO_TICKS( 3333UL ) )
#define mainAUTO_RELOAD_TIMER_PERIOD	( pdMS_TO_TICKS( 500UL ) )

/*-----------------------------------------------------------*/

/*
 * 两个定时器共用的回调函数。
 */
static void prvTimerCallback( TimerHandle_t xTimer );

/*-----------------------------------------------------------*/

/* 定时器句柄，由于要在回调函数中使用，所以这两个变量要定义成文件作用域. */
static TimerHandle_t xAutoReloadTimer, xOneShotTimer;

int main( void )
{
BaseType_t xTimer1Started, xTimer2Started;

	/* 创建单触发软定时器，并将定时器句柄保存在xOneShotTimer中 */
	xOneShotTimer = xTimerCreate( "OneShot",					/* 软定时器的名称 - FreeRTOS 没有使用. */
								  mainONE_SHOT_TIMER_PERIOD,	/* 以tick 为单位的时间周期. */
								  pdFALSE,						/* 设置 uxAutoRealod 为 pdFALSE ，创建的就是单次触发定时器  */
								  0,							/* 定时器ID初始化为 0. */
								  prvTimerCallback );			/* 软定时器的回调函数. */

	/* 创建自动加载软定时器，并将定时器句柄存在xAutoReloadTimer中 */
	xAutoReloadTimer = xTimerCreate( "AutoReload",					/* 软定时器的名称 - FreeRTOS 没有使用. */
									 mainAUTO_RELOAD_TIMER_PERIOD,	/* 以tick 为单位的时间周期. */
									 pdTRUE,						/* 设置 uxAutoRealod 为  pdTRUE,创建的就是自动加载定时器. */
									 0,								/* 定时器ID初始化为 0. */
									 prvTimerCallback );			/* 软定时器的回调函数. */

	/* 检查定时器是否都创建成功. */
	if( ( xOneShotTimer != NULL ) && ( xAutoReloadTimer != NULL ) )
	{
		/* 开启软定时器，使用阻塞时间0(非阻塞时间).
		由于调度还没有开始，所以这里指定的时间将会被忽略。
		（这里开启了定时器，并不表示定时器就立即运行了。
		它是在任务开始调度后才运行的）*/
		xTimer1Started = xTimerStart( xOneShotTimer, 0 );
		xTimer2Started = xTimerStart( xAutoReloadTimer, 0 );

		/*  xTimerStart()函数的使用了一个叫做(定时器命令队列)来实现的。并且当
		定时器命令队列满时，xTimerStart()将会失败。定时器服务任务将不会开始，
		直到调度器开启了.所以所有发送给命令队列的命令将会保持在队列中，直到调度
		器启动。检查两个xTimerStart()是否调用成功. */
		if( ( xTimer1Started == pdPASS ) && ( xTimer2Started == pdPASS ) )
		{
			/* 开启调度. */
			vTaskStartScheduler();
		}
	}

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void prvTimerCallback( TimerHandle_t xTimer )
{
TickType_t xTimeNow;
uint32_t ulExecutionCount;

	/* 当前软定时器的到期次数被存在定时器的id中，获取这个id值，并增加它，
	并将其保存为新的定时器ID.这个ID是一个void指针，所以把它强制转换成uint32_t */
	ulExecutionCount = ( uint32_t ) pvTimerGetTimerID( xTimer );
	ulExecutionCount++;
	vTimerSetTimerID( xTimer, ( void * ) ulExecutionCount );

	/* 获取当前的时钟节拍计数. */
	xTimeNow = xTaskGetTickCount();

    /* 单次触发定时器的句柄，在创建时存储在xOneShotTimer中。
	把本函数传入的句柄与xOneShotTimer进行比较，以检查确定是
	单次触发定时器，还是自动加载定时器时间到期，然后输出显示是
	哪个定时器执行了本回调函数.*/
	if( xTimer == xOneShotTimer )
	{
		vPrintStringAndNumber( "One-shot timer callback executing", xTimeNow );
	}
	else
	{
        /* xTimer不等于 xOneShotTimer,所以必定时自动加载定时器到期了 */
		vPrintStringAndNumber( "Auto-reload timer callback executing", xTimeNow );

		if( ulExecutionCount == 5 )
		{
			/* 自动重新加载定时器执行5后，将它停止。这个函数是在RTOS的守护任务
			的上下文中执行的，所以不要调用（任何）可能导致守护任务进入阻塞态的函数。
			因此阻塞时间用0. */
			xTimerStop( xTimer, 0 );
		}
	}
}
/*-----------------------------------------------------------*/

