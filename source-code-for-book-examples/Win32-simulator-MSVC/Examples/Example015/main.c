/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示例演示了定时器与按键结合使用，模拟控制开关灯的功能.
		当按键按下时，如果灯是灭的，那么就开启灯，并启动一次触发定时器，定时时间5秒后，将关闭灯.
		当按键按下时，如果灯是亮的，保持灯的状态不变，并复位单次触发定时器，定时时间5秒后，将关闭灯.    
*/

/* 标准头文件包含. */
#include <conio.h>

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

/* 示例包含头文件. */
#include "supporting_functions.h"

/* 单次触发时间周期. */
#define mainBACKLIGHT_TIMER_PERIOD		( pdMS_TO_TICKS( 5000UL ) )

/*-----------------------------------------------------------*/

/*
 * 定时器回调函数。.
 */
static void prvBacklightTimerCallback( TimerHandle_t xTimer );

/*
 * 在实际的项目环境中，一个真实的应用中一般可能是从中断中读取按键。
 * 这样就可以使得应用被事件驱动，就不用在按键没有按下时，
 * 浪费CPU时间去查询按键是否按下.
 * 在FreeRTOS的windows移植版本中，使用按键中断并不实际，所以vKeyHitTask()
 * 这个任务只是简单的循环去查询按键是否按下
 */
static void vKeyHitTask( void *pvParameters );

/*-----------------------------------------------------------*/

/* 本示例并没有真实的背光亮暗操作，所以下面的变量只是用于保持按键的亮暗状态 */
static BaseType_t xSimulatedBacklightOn = pdFALSE;

/* 软定时器用于关闭背光. */
static TimerHandle_t xBacklightTimer = NULL;

/*-----------------------------------------------------------*/

int main( void )
{
	/* 背光状态开始是关闭的. */
	xSimulatedBacklightOn = pdFALSE;

	/* 创建单触发软定时器，并将定时器句柄保存在xOneShotTimer中 */
	xBacklightTimer = xTimerCreate( "Backlight",				/* 软定时器的名称 - FreeRTOS 没有使用. */
									mainBACKLIGHT_TIMER_PERIOD,	/* 以tick 为单位的时间周期. */
									pdFALSE,					/* 设置 uxAutoRealod 为 pdFALSE ，创建的就是单次触发定时器. */
									0,							/* 定时器ID初始化为 0. */
									prvBacklightTimerCallback );/* 软定时器的回调函数. */

	/*
	* 在实际的项目环境中，一个真实的应用中一般可能是从中断中读取按键。
	* 这样就可以使得应用被事件驱动，就不用在按键没有按下时，
	* 浪费CPU时间去查询按键是否按下.
	* 在FreeRTOS的windows移植版本中，使用按键中断并不实际，所以vKeyHitTask()
	* 这个任务只是简单的循环去查询按键是否按下
	*/
	xTaskCreate( vKeyHitTask, "Key poll", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );

	/* 开启定时器. */
	xTimerStart( xBacklightTimer, 0 );

	/* 开启调度. */
	vTaskStartScheduler();

	/* 与之前的示例一样，vTaskStartScheduler()应该永远不会返回，所以下面的代码不会被运行 */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void prvBacklightTimerCallback( TimerHandle_t xTimer )
{
TickType_t xTimeNow = xTaskGetTickCount();

	/* 背光定时器时间到，关闭灯光 */
	xSimulatedBacklightOn = pdFALSE;

	/* 打印背光关闭时间. */
	vPrintStringAndNumber( "Timer expired, turning backlight OFF at time\t", xTimeNow );
}
/*-----------------------------------------------------------*/

static void vKeyHitTask( void *pvParameters )
{
const TickType_t xShortDelay = pdMS_TO_TICKS( 50 );
extern BaseType_t xKeyPressesStopApplication;
TickType_t xTimeNow;

	/* 这个示例要使用按键，所以这里设置该变量的值以阻止应用退出 */
	xKeyPressesStopApplication = pdFALSE;

	vPrintString( "Press a key to turn the backlight on.\r\n" );

	/*
	* 在实际的项目环境中，一个真实的应用中一般可能是从中断中读取按键。
	* 这样就可以使得应用被事件驱动，就不用在按键没有按下时，
	* 浪费CPU时间去查询按键是否按下.
	* 在FreeRTOS的windows移植版本中，使用按键中断并不实际，所以vKeyHitTask()
	* 这个任务只是简单的循环去查询按键是否按下
	*/
	for( ;; )
	{
		/* 有键按下吗？ */
		if( _kbhit() != 0 )
		{
			/* 记录一下按键按下的时间 */
			xTimeNow = xTaskGetTickCount();

			/* 按键按下了. */
			if( xSimulatedBacklightOn == pdFALSE )
			{
				/* 如果背光是关的，那么就打开它，并记录背光点亮的时间. */
				xSimulatedBacklightOn = pdTRUE;
				vPrintStringAndNumber( "Key pressed, turning backlight ON at time\t", xTimeNow );
			}
			else
			{
				/* 背光灯已经点亮，所以输出一个背光灯即将关闭的消息，并记录关闭的时间 */
				vPrintStringAndNumber( "Key pressed, resetting software timer at time\t", xTimeNow );
			}

			/* 复位软定时器，如果背光之前是关闭的，那么本次调用将开启定时器。
			 如果背光灯是亮的，那么这将复位定时器.
			 在实际的应用中将是在一个中断中读取按键。
			 如果这个函数是一个中断服务函数，那么这里应该用xTimerResetFromISR()
			 函数替换 xTimerReset() */
			xTimerReset( xBacklightTimer, xShortDelay );

			/* 读取并丢弃按下的按键值. */
			( void ) _getch();
		}

		/* 用于减慢查询速度(加个延时，以免循环跑的太快了). */
		vTaskDelay( xShortDelay );
	}
}
/*-----------------------------------------------------------*/

