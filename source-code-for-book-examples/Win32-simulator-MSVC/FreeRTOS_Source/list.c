/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    http://www.FreeRTOS.org/FAQHelp.html - 有问题吗? 阅读FAQ页面
    "我的应用不能运行, 有可能哪里出氏了?". 你定义了configASSERT()吗?

    http://www.FreeRTOS.org/support - 作为收到如此高质量软年的回馈，我们邀请你
    通过参与支持论坛，来支持我们的全球社区

    http://www.FreeRTOS.org/training - 投资于培训可以让你的团队尽可能早地提高生产力。
    现在，您可以直接从Real Time Engineers Ltd首席执行官Richard Barry
    那里获得FreeRTOS培训，他是全球领先RTO的权威机构。
    
    http://www.FreeRTOS.org/plus - 一系列FreeRTOS生态系统产品，
    包括FreeRTOS+Trace——一个不可或缺的生产力工具，一个与DOS兼容的FAT文件系统，
    以及我们的微型线程感知UDP/IP协议栈。

    http://www.FreeRTOS.org/labs - FreeRTOS新产品的孵化地。快来试试FreeRTOS+TCP，
    我们为FreeRTOS开发的新的开源TCP/IP协议栈。

    http://www.OpenRTOS.com - Real Time Engineers ltd.向High Integrity Systems ltd.
    授权FreeRTOS以OpenRTOS品牌销售。低成本的OpenRTOS许可证提供有票证的支持、赔偿和商业中间件。

    http://www.SafeRTOS.com - 高完整性系统还提供安全工程和独立SIL3认证版本，
    用于需要可证明可靠性的安全和关键任务应用。

    1 tab == 4 spaces!
*/


#include <stdlib.h>
#include "FreeRTOS.h"
#include "list.h"

/*-----------------------------------------------------------
 * 公开的列表API文档在 list.h 中
 *----------------------------------------------------------*/

void vListInitialise( List_t * const pxList )
{
	/* 列表结构中包含一个表示列表尾的列表项.初始化列表，
	   在列表尾插入一个唯一的列表entry. */
	pxList->pxIndex = ( ListItem_t * ) &( pxList->xListEnd );			/*lint !e826 !e740 采用mini-list结构作为列表端，以节省RAM。经检查这是有效的. */

	/* 列表结尾的值表示列表最大可以包含的节点值，要确保它始终处于列表尾*/
	pxList->xListEnd.xItemValue = portMAX_DELAY;

	/* 列表的后置和前置指针指向它自己，由此可知列表为空
	when the list is empty. */
	pxList->xListEnd.pxNext = ( ListItem_t * ) &( pxList->xListEnd );	/*lint !e826 !e740 采用mini-list结构作为列表端，以节省RAM。经检查这是有效的. */
	pxList->xListEnd.pxPrevious = ( ListItem_t * ) &( pxList->xListEnd );/*lint !e826 !e740 采用mini-list结构作为列表端，以节省RAM。经检查这是有效的. */

	pxList->uxNumberOfItems = ( UBaseType_t ) 0U;

	/* 如果configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES为1，
	   则把一个已知值写入列表.
       这个配置用于检测列表的值是否被破坏，即完整性检测。
	 */
	listSET_LIST_INTEGRITY_CHECK_1_VALUE( pxList );
	listSET_LIST_INTEGRITY_CHECK_2_VALUE( pxList );
}
/*-----------------------------------------------------------*/

void vListInitialiseItem( ListItem_t * const pxItem )
{
	/* 确保列表项没有记录在一个列表中(不属于任何列表). */
	pxItem->pvContainer = NULL;

	/* 如果configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES为1，
	   则把一个已知值写入列表.
       这个配置用于检测列表的值是否被破坏，即完整性检测。*/
	listSET_FIRST_LIST_ITEM_INTEGRITY_CHECK_VALUE( pxItem );
	listSET_SECOND_LIST_ITEM_INTEGRITY_CHECK_VALUE( pxItem );
}
/*-----------------------------------------------------------*/

void vListInsertEnd( List_t * const pxList, ListItem_t * const pxNewListItem )
{
ListItem_t * const pxIndex = pxList->pxIndex;

	/* 当configASSERT()也配置的情况下才有效。这些检测可以捕获出链表的数据
	   在内存中被复盖。但不能捕获由于错误的配置和使用FreeRTOS导致的数据错误*/
	listTEST_LIST_INTEGRITY( pxList );
	listTEST_LIST_ITEM_INTEGRITY( pxNewListItem );

	/* 插入一个新的项到pxList,但不对其做排序,
	makes the new list item the last item to be removed by a call to
	listGET_OWNER_OF_NEXT_ENTRY(). */
	pxNewListItem->pxNext = pxIndex;
	pxNewListItem->pxPrevious = pxIndex->pxPrevious;

	/* 仅在决策覆盖测试期间使用. */
	mtCOVERAGE_TEST_DELAY();

	pxIndex->pxPrevious->pxNext = pxNewListItem;
	pxIndex->pxPrevious = pxNewListItem;

	/* 记住该项所在的列表. */
	pxNewListItem->pvContainer = ( void * ) pxList;

	( pxList->uxNumberOfItems )++;
}
/*-----------------------------------------------------------*/

void vListInsert( List_t * const pxList, ListItem_t * const pxNewListItem )
{
ListItem_t *pxIterator;
const TickType_t xValueOfInsertion = pxNewListItem->xItemValue;

	/* 当configASSERT()也配置的情况下才有效。这些检测可以捕获出链表的数据
	   在内存中被复盖。但不能捕获由于错误的配置和使用FreeRTOS导致的数据错误*/
	listTEST_LIST_INTEGRITY( pxList );
	listTEST_LIST_ITEM_INTEGRITY( pxNewListItem );

	/* 插入一个新的列表项到列表，按它的xItemValue值排序
	如果列表已包含一个值相同的列表项，那么新的列表项放置在它的后面。
	这是为了储存在就绪列表中的TCB(捅有相同的xItemValue值)共享CPU.
	但时，如果xItemValue与最后的标记值相同，下面的迭代循环将不会结束。
	因此有必要先对它的值做检测，所以算法对此做了一点点调整。*/
	if( xValueOfInsertion == portMAX_DELAY )
	{
		pxIterator = pxList->xListEnd.pxPrevious;
	}
	else
	{
		/* *** 注意 ***********************************************************
		如果发现你的应用是在这里崩溃的，那么有可能是如下原因
		另外还可参考 http://www.freertos.org/FAQHelp.html 获取更多提示
		确保configASSERT()被定义了,网址：
		http://www.freertos.org/a00110.html#configASSERT

			1) 栈溢出 -
			   参才 http://www.freertos.org/Stacks-and-stack-overflow-checking.html
			2) 不正确的中断优先级赋值,尤其是在Contex-M部分，数字值越高实际表示
			   中断优先级越低，这与我们的直觉是相反的.  参才
			   http://www.freertos.org/RTOS-Cortex-M3-M4.html 和
			   configMAX_SYSCALL_INTERRUPT_PRIORITY在网址中的定义
			   http://www.freertos.org/a00110.html
			3) 在非临界区调用了一个API函数，或调度器挂起，或者在中断中调用了
			   一个不是以"FromISR"结尾的API函数.
			4) 使用了没有初始化的队列或信号量，或者还没有启动调度器
			   (调度器vTaskStartScheduler()调用之前，触发了中断).
		**********************************************************************/

		for( pxIterator = ( ListItem_t * ) &( pxList->xListEnd ); pxIterator->pxNext->xItemValue <= xValueOfInsertion; pxIterator = pxIterator->pxNext ) /*lint !e826 !e740 The mini list structure is used as the list end to save RAM.  This is checked and valid. */
		{
			/* 这里不需要做什么，只是为了迭代到插入位置 */
		}
	}

	pxNewListItem->pxNext = pxIterator->pxNext;
	pxNewListItem->pxNext->pxPrevious = pxNewListItem;
	pxNewListItem->pxPrevious = pxIterator;
	pxIterator->pxNext = pxNewListItem;

	/* 记住该项所在的列表，这将允许后续可以快速的移除该项. */
	pxNewListItem->pvContainer = ( void * ) pxList;

	( pxList->uxNumberOfItems )++;
}
/*-----------------------------------------------------------*/

UBaseType_t uxListRemove( ListItem_t * const pxItemToRemove )
{
/* 列表项知道自己所在的列表，从列表项中获取列表 */
List_t * const pxList = ( List_t * ) pxItemToRemove->pvContainer;

	pxItemToRemove->pxNext->pxPrevious = pxItemToRemove->pxPrevious;
	pxItemToRemove->pxPrevious->pxNext = pxItemToRemove->pxNext;

	/* 仅在决策覆盖测试期间使用. */
	mtCOVERAGE_TEST_DELAY();

	/* 确保列表向左指向有效. */
	if( pxList->pxIndex == pxItemToRemove )
	{
		pxList->pxIndex = pxItemToRemove->pxPrevious;
	}
	else
	{
		mtCOVERAGE_TEST_MARKER();
	}

	pxItemToRemove->pvContainer = NULL;
	( pxList->uxNumberOfItems )--;

	return pxList->uxNumberOfItems;
}
/*-----------------------------------------------------------*/

